//1.
db.users.insertMany([
        {
            firstName: "Stephen",
            lastName: "Strange",
            email: "drStrange@mail.com",
            password: "eyeofagamotto",
            isAdmin: false
        },
        {
            firstName: "Tony",
            lastName: "Stark",
            email: "Stark@mail.com",
            password: "ILoveYou3000",
            isAdmin: false
        },
        {
            firstName: "Peter",
            lastName: "Parker",
            email: "friendlyneighborhood@mail.com",
            password: "Spidey",
            isAdmin: false
        },
        {
            firstName: "Steve",
            lastName: "Rogers",
            email: "Capt@mail.com",
            password: "Iamworthy",
            isAdmin: false
        },
        {
            firstName: "Thor",
            lastName: "Odinson",
            email: "Thor@mail.com",
            password: "mjolnir",
            isAdmin: false
        }])

db.courses.insertMany ([
        {
            "name": "Fighting 101",
            "price": 5000,
            "description": "Basic Fighting Techniques",
            "isActive": false
                },
        {
            "name": "Avengers 101",
            "price": 2000,
            "description": "Avengers History and Mission",
            "isActive": false
        },
        {
            "name": "Disaster and Risk Management 101",
            "price": 2500,
            "description": "How to manage risk and make a decision",
            "isActive": false
        }
    ])

//2.
db.users.find(
        {
            isAdmin: false
        }),

//3.

db.users.updateOne(
    {
        firstName:"Stephen"
    },  
    {
        $set: {
            isAdmin: true
        }
    })
        
        
db.courses.updateOne(
    {
        name:"Fighting 101"
    },  
    {
        $set: {
            isActive: true
        }
    })  

//4.
db.courses.deleteMany({isActive: false})